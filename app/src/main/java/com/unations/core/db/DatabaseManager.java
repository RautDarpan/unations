package com.unations.core.db;

import android.content.Context;
import android.util.Log;


import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.DBUser;

import java.sql.SQLException;
import java.util.List;


/**
 * Created by pleximus on 25/04/17.
 */

public class DatabaseManager {

    private static final String TAG = DatabaseManager.class.getName();

    private static DatabaseManager dbManagerInstance;


    DatabaseHelper dbHelper;

    public DatabaseManager(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    /**
     * returns the instance of the class
     *
     * @param context
     * @return instance
     */
    static public DatabaseManager getInstance(Context context) {
        if (dbManagerInstance == null) {
            dbManagerInstance = new DatabaseManager(context);
        }
        return dbManagerInstance;
    }

    /**
     * returns the Helper instance
     *
     * @return
     */
    public DatabaseHelper getHelper() {
        return dbHelper;
    }

    /** User **/

    /**
     * add or update user details in database
     *
     * @param user
     */
    public boolean addUser(DBUser user) {
        if (user == null) {
            return false;
        } else {
            try {
                getHelper().getDbUserDao().createOrUpdate(user);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * get Nation Details
     *
     * @param dbCountryDetail
     * @return
     */
    public boolean addNationToDb(DBCountryDetail dbCountryDetail) {
        if (dbCountryDetail == null) {
            return false;
        } else {
            try {
                getHelper().getDbCountryDetailsDao().createOrUpdate(dbCountryDetail);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * get all countries
     *
     * @return
     */
    public List<DBCountryDetail> getAllcountries() {
        List<DBCountryDetail> dbCountryDetails = null;
        try {
            dbCountryDetails = getHelper().getDbCountryDetailsDao().queryForAll();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
        }
        if (dbCountryDetails == null || dbCountryDetails.size() == 0)
            return null;
        else
            return dbCountryDetails;
    }

    public List<DBCountryDetail> getCountriesForQuery(String query) {
        List<DBCountryDetail> dbCountryDetails = null;
        try {
            dbCountryDetails = getHelper().getDbCountryDetailsDao().queryBuilder().where().eq("countryName", query).query();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
        }
        if (dbCountryDetails == null || dbCountryDetails.size() == 0)
            return null;
        else
            return dbCountryDetails;
    }

}
