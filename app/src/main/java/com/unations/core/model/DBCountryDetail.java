package com.unations.core.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by 3543 on 08-11-2017.
 */

@DatabaseTable(tableName = "DBCountryDetails")
public class DBCountryDetail implements Serializable{

    @DatabaseField(id = true)
    private String countryId;
    @DatabaseField
    private String countryName;
    @DatabaseField
    private String countryPopulation;
    @DatabaseField
    private String TimeZone;
    @DatabaseField
    private String countryFlagImg;
    @DatabaseField
    private String countryCapital;
    @DatabaseField
    private String nativeName;
    @DatabaseField
    private String borders;
    @DatabaseField
    private String callingCode;
    @DatabaseField
    private String topLevelDomain;
    @DatabaseField
    private String subRegion;
    @DatabaseField
    private String region;

    public String getCountryCapital() {
        return countryCapital;
    }

    public void setCountryCapital(String countryCapital) {
        this.countryCapital = countryCapital;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public String getBorders() {
        return borders;
    }

    public void setBorders(String borders) {
        this.borders = borders;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getTopLevelDomain() {
        return topLevelDomain;
    }

    public void setTopLevelDomain(String topLevelDomain) {
        this.topLevelDomain = topLevelDomain;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(String subRegion) {
        this.subRegion = subRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryPopulation() {
        return countryPopulation;
    }

    public void setCountryPopulation(String countryPopulation) {
        this.countryPopulation = countryPopulation;
    }

    public String getTimeZone() {
        return TimeZone;
    }

    public void setTimeZone(String timeZone) {
        TimeZone = timeZone;
    }

    public String getCountryFlagImg() {
        return countryFlagImg;
    }

    public void setCountryFlagImg(String countryFlagImg) {
        this.countryFlagImg = countryFlagImg;
    }
}
