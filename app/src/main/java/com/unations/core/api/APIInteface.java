package com.unations.core.api;

import com.unations.core.model.NationListResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by pleximus on 21/04/17.
 */

public interface APIInteface {

    @GET("v2/all")
    Observable<List<NationListResponse>> getAllNationsList();
}
