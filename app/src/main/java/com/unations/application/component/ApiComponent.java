package com.unations.application.component;


import com.unations.application.module.ApiModule;
import com.unations.application.module.DatabaseModule;
import com.unations.application.module.RxModule;
import com.unations.application.scope.AppScope;
import com.unations.core.api.APIInteface;
import com.unations.core.bus.RxBus;
import com.unations.core.db.DatabaseManager;

import dagger.Component;

/**
 * Created by pleximus on 21/04/17.
 */
@AppScope
@Component(modules = {ApiModule.class, RxModule.class, DatabaseModule.class}, dependencies = {NetworkComponent.class})
public interface ApiComponent {

    APIInteface provideApiInterface();

    RxBus rxBus();

    DatabaseManager databaseManager();

}
