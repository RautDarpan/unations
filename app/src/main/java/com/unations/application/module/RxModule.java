package com.unations.application.module;


import com.unations.core.bus.RxBus;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pleximus on 25/04/17.
 */
@Module
public class RxModule {


    @Provides
    RxBus provideRxBus() {
        return new RxBus();
    }
}
