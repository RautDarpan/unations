package com.unations.application.module;

import com.unations.core.api.APIInteface;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by pleximus on 21/04/17.
 */
@Module
public class ApiModule {

    @Provides
    APIInteface providesRetrofitApiService(Retrofit retrofit) {
        return retrofit.create(APIInteface.class);
    }

}
