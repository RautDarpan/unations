package com.unations;

import android.app.Application;
import android.content.Context;

import com.unations.application.component.ApiComponent;
import com.unations.application.component.DaggerApiComponent;
import com.unations.application.component.DaggerNetworkComponent;
import com.unations.application.component.NetworkComponent;
import com.unations.application.module.DatabaseModule;
import com.unations.application.module.NetworkModule;
import com.unations.application.module.RxModule;
import com.unations.core.bus.RxBus;

/**
 * Created by 3543 on 08-11-2017.
 */

public class NationApplication extends Application {

    private static final String BASE_URL = "https://restcountries.eu/rest/";
    private static RxBus rxBus;
    private Context context;
    private static ApiComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        rxBus = new RxBus();
        resolveDependency();
    }

    /**
     * Method to resolve Application Scope Dependencies
     */
    private void resolveDependency() {
        if (appComponent == null) {
            appComponent = DaggerApiComponent.builder()
                    .networkComponent(getNetworkComponent())
                    .databaseModule(new DatabaseModule(this))
                    .rxModule(new RxModule())
                    .build();
        }
    }

    /**
     * get Rxbus instance
     *
     * @return
     */
    public static RxBus bus() {
        if (rxBus == null) {
            return new RxBus();
        }
        return rxBus;
    }


    /**
     * get AppComponent Instance
     *
     * @return
     */
    public static ApiComponent getAppComponent() {
        return appComponent;
    }

    /**
     * Initialise Network Component
     *
     * @return
     */
    public NetworkComponent getNetworkComponent() {
        return DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule(BASE_URL))
                .build();
    }



}
