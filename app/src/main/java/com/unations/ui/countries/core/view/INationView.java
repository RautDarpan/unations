package com.unations.ui.countries.core.view;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;

/**
 * Created by 3543 on 08-11-2017.
 */

public interface INationView {

    void onError();

    void getNationList();

    void onDisplay();

    void countryList(List<DBCountryDetail> nationListResponses);

    void onLoadNationList(List<DBCountryDetail> nationListResponses);
}
