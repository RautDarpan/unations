package com.unations.ui.countries.adapter;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.unations.R;
import com.unations.core.model.DBCountryDetail;
import com.unations.utils.SvgDecoder;
import com.unations.utils.SvgDrawableTranscoder;
import com.unations.utils.SvgSoftwareLayerSetter;

import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pleximus on 06/05/17.
 */

public class NationListAdapter extends RecyclerView.Adapter<NationListAdapter.ContactViewHolder> {


    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private Context context;
    private List<DBCountryDetail> dbCountryDetails;
    private ConactsListClickListner clickListner;
    private final LayoutInflater mInflater;

    public NationListAdapter(Context context, ConactsListClickListner clickListner, List<DBCountryDetail> dbCountryDetails, LayoutInflater mInflater) {
        this.context = context;
        this.clickListner = clickListner;
        this.dbCountryDetails = dbCountryDetails;
        this.mInflater = mInflater;
    }

    @Override
    public NationListAdapter.ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactViewHolder(mInflater.inflate(R.layout.row_nation, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        DBCountryDetail dbCountryDetail = dbCountryDetails.get(position);
        if (dbCountryDetail.getCountryName() != null) {
            holder.lblCountryName.setText(dbCountryDetail.getCountryName());
        }
        holder.lblCountryCaptial.setText("@CAPITAL : " + dbCountryDetail.getCountryCapital());

        Uri uri = Uri.parse(dbCountryDetail.getCountryFlagImg());


        requestBuilder = Glide.with(context)
                .using(Glide.buildStreamModelLoader(Uri.class, context), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());

        if (dbCountryDetail.getCountryFlagImg() != null) {
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    // SVG cannot be serialized so it's not worth to cache it
                    .load(uri)
                    .into(holder.ivFlag);
        }

    }


    @Override
    public int getItemCount() {
        if (dbCountryDetails != null && dbCountryDetails.size() > 0) {
            return dbCountryDetails.size();
        }
        return 0;
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.lbl_country_name)
        TextView lblCountryName;
        @BindView(R.id.lbl_country_capital)
        TextView lblCountryCaptial;
        @BindView(R.id.iv_flag)
        ImageView ivFlag;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListner.onNationClick(getAdapterPosition(), dbCountryDetails.get(getAdapterPosition()));
        }
    }

    public interface ConactsListClickListner {
        void onNationClick(int position, DBCountryDetail dbCountryDetail);
    }

}
