package com.unations.ui.countries.core.model;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;

public interface INationModel {

    void saveNationsDetailsToDb(List<NationListResponse> nationListResponsesa);

    List<DBCountryDetail> getCountries();
}