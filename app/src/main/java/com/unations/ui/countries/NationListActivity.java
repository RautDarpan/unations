package com.unations.ui.countries;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;

import com.unations.NationApplication;
import com.unations.R;
import com.unations.core.api.APIInteface;
import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;
import com.unations.ui.countries.adapter.NationListAdapter;
import com.unations.ui.countries.core.presenter.INationPresenter;
import com.unations.ui.countries.core.presenter.NationPresenterImpl;
import com.unations.ui.countries.core.view.INationView;
import com.unations.ui.countries.dagger.component.DaggerNationComponent;
import com.unations.ui.countries.dagger.module.NationListModule;
import com.unations.ui.detail.CountryDetailsActivity;
import com.unations.utils.RxSearchObservable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class NationListActivity extends AppCompatActivity implements INationView, NationListAdapter.ConactsListClickListner {

    private NationListAdapter nationListAdapter;
    private Context context;
    private List<DBCountryDetail> dbCountryDetails;
    @Inject
    INationPresenter iNationPresenter;
    @Inject
    NationPresenterImpl nationPresenter;
    @Inject
    APIInteface apiInteface;

    @BindView(R.id.rv_nations)
    RecyclerView recyclerView;
    @BindView(R.id.searchView)
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nation_list);
        ButterKnife.bind(this);
        context = this;

       // Initialise Dagger
        DaggerNationComponent.builder()
                .apiComponent(NationApplication.getAppComponent())
                .nationListModule(new NationListModule(this))
                .build()
                .inject(this);

        nationPresenter.getNationList();
        setUpSearchObservable();
    }

    @Override
    public void onError() {

    }

    private void setUpSearchObservable() {
        RxSearchObservable.fromView(searchView)
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String text) throws Exception {
                            return true;
                    }
                })
                .distinctUntilChanged()
                .switchMap(new Function<String, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(String query) throws Exception {
                        return dataFromNetwork(query);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String result) throws Exception {
                        List<DBCountryDetail> queryResultsList = new ArrayList<>();
                        if (dbCountryDetails.size() > 0) {

                            for (DBCountryDetail countryDetail : dbCountryDetails) {
                                if (countryDetail.getCountryName().toLowerCase().contains(result.toLowerCase())) {
                                    queryResultsList.add(countryDetail);
                                }
                            }
                            if (queryResultsList.size() > 0)
                                nationPresenter.onLoadSearchList(queryResultsList);
                            else
                                nationPresenter.onLoadSearchList(dbCountryDetails);
                        }

                    }
                });
    }


    private Observable<String> dataFromNetwork(final String query) {
        return Observable.just(true)
                .map(new Function<Boolean, String>() {
                    @Override
                    public String apply(@NonNull Boolean value) throws Exception {
                        return query;
                    }
                });
    }

    /**
     * get all the Nations List from the Link
     */
    @Override
    public void getNationList() {
        nationPresenter.addDisposableObserver(apiInteface.getAllNationsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Object>() {
                    @Override
                    public void onNext(Object o) {
                        nationPresenter.saveNationToDb((List<NationListResponse>) o);
                        nationPresenter.onLoadNationList((List<NationListResponse>) o);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void onDisplay() {

    }

    @Override
    public void countryList(List<DBCountryDetail> nationListResponses) {
        dbCountryDetails = new ArrayList<>();
        dbCountryDetails.addAll(nationListResponses);
    }

    /**
     * display all countries in list
     *
     * @param nationListResponses
     */
    @Override
    public void onLoadNationList(List<DBCountryDetail> nationListResponses) {
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        nationListAdapter = new NationListAdapter(context, this, nationListResponses, getLayoutInflater());
        recyclerView.setAdapter(nationListAdapter);
    }

    @Override
    public void onNationClick(int position, DBCountryDetail details) {
        Intent intent = new Intent(getBaseContext(), CountryDetailsActivity.class);
        intent.putExtra("details", details);
        startActivity(intent);
    }
}
