package com.unations.ui.countries.core.presenter;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;
import com.unations.ui.base.BasePresenter;
import com.unations.ui.countries.core.model.INationModel;
import com.unations.ui.countries.core.model.NationModelModel;
import com.unations.ui.countries.core.view.INationView;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * Created by 3543 on 08-11-2017.
 */

public class NationPresenterImpl extends BasePresenter implements INationPresenter {

    public INationView iNationView;
    public INationModel iNationModel;

    public NationPresenterImpl(INationView iNationView, NationModelModel nationModelModel) {
        this.iNationView = iNationView;
        this.iNationModel = nationModelModel;
    }

    @Override
    public void getNationList() {
        iNationView.getNationList();
    }

    @Override
    public void saveNationToDb(List<NationListResponse> nationListResponses) {
        iNationModel.saveNationsDetailsToDb(nationListResponses);
    }

    @Override
    public void onLoadNationList(List<NationListResponse> nationListResponses) {
        iNationView.countryList(iNationModel.getCountries());
        iNationView.onLoadNationList(iNationModel.getCountries());
    }

    @Override
    public void onLoadSearchList(List<DBCountryDetail> dbCountryDetails) {
        iNationView.onLoadNationList(dbCountryDetails);
    }

    /**
     * add disposable
     *
     * @param disposable
     */
    public void addDisposableObserver(Disposable disposable) {
        addDisposable(disposable);
    }
}
