package com.unations.ui.countries.dagger.module;


import com.unations.core.db.DatabaseManager;
import com.unations.ui.countries.core.model.NationModelModel;
import com.unations.ui.countries.core.presenter.INationPresenter;
import com.unations.ui.countries.core.presenter.NationPresenterImpl;
import com.unations.ui.countries.core.view.INationView;
import com.unations.ui.countries.dagger.scope.NationListScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pleximus on 06/05/17.
 */
@Module
public class NationListModule {

    private INationView iNationView;

    public NationListModule(INationView iNationView) {
        this.iNationView = iNationView;
    }

    @NationListScope
    @Provides
    INationView providesContactView() {
        return iNationView;
    }

    @NationListScope
    @Provides
    INationPresenter providesContactsPresneter(INationView iNationView, NationModelModel nationModelModel) {
        return new NationPresenterImpl(iNationView, nationModelModel);
    }

    @NationListScope
    @Provides
    NationPresenterImpl providesContactsPresenterImpl(INationView iNationView, NationModelModel nationModelModel) {
        return new NationPresenterImpl(iNationView, nationModelModel);
    }

    @NationListScope
    @Provides
    NationModelModel providesUserModel(DatabaseManager databaseManager) {
        return new NationModelModel(databaseManager);
    }


}
