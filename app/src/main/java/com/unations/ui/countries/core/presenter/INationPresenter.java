package com.unations.ui.countries.core.presenter;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;

/**
 * Created by 3543 on 08-11-2017.
 */

public interface INationPresenter {

    void getNationList();

    void saveNationToDb(List<NationListResponse> nationListResponses);

    void onLoadNationList(List<NationListResponse> nationListResponses);

    void onLoadSearchList(List<DBCountryDetail> dbCountryDetails);
}
