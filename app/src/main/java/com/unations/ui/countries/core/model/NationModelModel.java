package com.unations.ui.countries.core.model;


import com.unations.core.db.DatabaseManager;
import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;
import java.util.StringTokenizer;

public class NationModelModel implements INationModel {

    private DatabaseManager dbManager;

    public NationModelModel(DatabaseManager dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public void saveNationsDetailsToDb(List<NationListResponse> nationListResponsesa) {
        for (NationListResponse nationListResponse : nationListResponsesa) {
            DBCountryDetail dbCountryDetail = new DBCountryDetail();
            dbCountryDetail.setCountryFlagImg(nationListResponse.getFlag());
            dbCountryDetail.setCountryName(nationListResponse.getName());
            dbCountryDetail.setTimeZone(String.valueOf(nationListResponse.getTimezones()));
            dbCountryDetail.setCountryCapital(nationListResponse.getCapital());
            dbCountryDetail.setCountryPopulation(String.valueOf(nationListResponse.getPopulation()));
            if (nationListResponse.getTopLevelDomain() != null && nationListResponse.getTopLevelDomain().size() > 0) {
                StringBuilder domains = new StringBuilder();
                for (String domain : nationListResponse.getTopLevelDomain()) {
                    if (nationListResponse.getTopLevelDomain().size() > 1) {
                        domains.append(domain + ", ");
                    } else {
                        domains.append(domain);
                    }
                }
                dbCountryDetail.setTopLevelDomain(domains.toString());
            }
            if (nationListResponse.getBorders() != null && nationListResponse.getBorders().size() > 0) {
                StringBuilder borders = new StringBuilder();
                for (String border : nationListResponse.getBorders()) {
                    if (nationListResponse.getBorders().size() > 1) {
                        borders.append(border + ", ");
                    } else {
                        borders.append(border);
                    }

                }
                dbCountryDetail.setBorders(borders.toString());
            }
            if (nationListResponse.getCallingCodes() != null && nationListResponse.getCallingCodes().size() > 0) {
                StringBuilder codes = new StringBuilder();
                for (String code : nationListResponse.getCallingCodes()) {
                    if (nationListResponse.getCallingCodes().size() > 1) {
                        codes.append(code + ", ");
                    } else {
                        codes.append(code);
                    }
                }
                dbCountryDetail.setCallingCode(codes.toString());
            }
            dbCountryDetail.setSubRegion(nationListResponse.getSubregion());
            dbCountryDetail.setRegion(nationListResponse.getRegion());
            dbManager.addNationToDb(dbCountryDetail);
        }
    }

    @Override
    public List<DBCountryDetail> getCountries() {
        return dbManager.getAllcountries();
    }
}