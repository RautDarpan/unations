package com.unations.ui.countries.dagger.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by pleximus on 06/05/17.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface NationListScope {

}
