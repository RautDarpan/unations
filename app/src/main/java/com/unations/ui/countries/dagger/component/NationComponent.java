package com.unations.ui.countries.dagger.component;

import com.unations.application.component.ApiComponent;
import com.unations.ui.countries.NationListActivity;
import com.unations.ui.countries.dagger.module.NationListModule;
import com.unations.ui.countries.dagger.scope.NationListScope;

import dagger.Component;

/**
 * Created by pleximus on 06/05/17.
 */
@NationListScope
@Component(modules = NationListModule.class, dependencies = ApiComponent.class)
public interface NationComponent {
    void inject(NationListActivity nationListActivity);
}
