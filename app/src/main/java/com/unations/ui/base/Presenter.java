package com.unations.ui.base;

/**
 * Created by pleximus on 24/04/17.
 */

public interface Presenter {

    void onCreate();

    void onPause();

    void onResume();

    void onDestroy();
}
