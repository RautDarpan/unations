package com.unations.ui.detail.module;


import com.unations.core.db.DatabaseManager;

import com.unations.ui.countries.dagger.scope.NationListScope;
import com.unations.ui.detail.core.model.DetailsModel;
import com.unations.ui.detail.core.presenter.DetailsPresenterImpl;
import com.unations.ui.detail.core.presenter.IDetailsPresenter;
import com.unations.ui.detail.core.view.IDetailsView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pleximus on 06/05/17.
 */
@Module
public class DetailsModule {

    private IDetailsView IDetailsView;

    public DetailsModule(IDetailsView IDetailsView) {
        this.IDetailsView = IDetailsView;
    }

    @NationListScope
    @Provides
    IDetailsView providesContactView() {
        return IDetailsView;
    }

    @NationListScope
    @Provides
    IDetailsPresenter providesContactsPresneter(IDetailsView IDetailsView, DetailsModel nationModelModel) {
        return new DetailsPresenterImpl(IDetailsView, nationModelModel);
    }

    @NationListScope
    @Provides
    DetailsPresenterImpl providesContactsPresenterImpl(IDetailsView IDetailsView, DetailsModel nationModelModel) {
        return new DetailsPresenterImpl(IDetailsView, nationModelModel);
    }

    @NationListScope
    @Provides
    DetailsModel providesUserModel(DatabaseManager databaseManager) {
        return new DetailsModel(databaseManager);
    }


}
