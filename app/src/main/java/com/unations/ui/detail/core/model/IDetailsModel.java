package com.unations.ui.detail.core.model;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;

public interface IDetailsModel {

    void saveNationsDetailsToDb(List<NationListResponse> nationListResponsesa);

    List<DBCountryDetail> getCountries();
}