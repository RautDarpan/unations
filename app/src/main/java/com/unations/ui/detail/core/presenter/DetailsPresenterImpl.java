package com.unations.ui.detail.core.presenter;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;
import com.unations.ui.base.BasePresenter;
import com.unations.ui.detail.core.model.IDetailsModel;
import com.unations.ui.detail.core.model.DetailsModel;
import com.unations.ui.detail.core.view.IDetailsView;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * Created by 3543 on 08-11-2017.
 */

public class DetailsPresenterImpl extends BasePresenter implements IDetailsPresenter {

    public IDetailsView iDetailsView;
    public IDetailsModel iDetailsModel;

    public DetailsPresenterImpl(IDetailsView iDetailsView, DetailsModel detailsModel) {
        this.iDetailsView = iDetailsView;
        this.iDetailsModel = detailsModel;
    }

    @Override
    public void displayDetails(DBCountryDetail dbCountryDetails) {
        iDetailsView.onDisplay(dbCountryDetails);
    }
}
