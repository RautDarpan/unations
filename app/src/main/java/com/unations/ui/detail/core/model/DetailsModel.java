package com.unations.ui.detail.core.model;


import com.unations.core.db.DatabaseManager;
import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;

public class DetailsModel implements IDetailsModel {

    private DatabaseManager dbManager;

    public DetailsModel(DatabaseManager dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public void saveNationsDetailsToDb(List<NationListResponse> nationListResponsesa) {
        for (NationListResponse nationListResponse : nationListResponsesa) {
            DBCountryDetail dbCountryDetail = new DBCountryDetail();
            dbCountryDetail.setCountryFlagImg(nationListResponse.getFlag());
            dbCountryDetail.setCountryName(nationListResponse.getName());
            dbCountryDetail.setTimeZone(String.valueOf(nationListResponse.getTimezones()));
            dbCountryDetail.setCountryCapital(nationListResponse.getCapital());
            if (nationListResponse.getTopLevelDomain() != null && nationListResponse.getTopLevelDomain().size() > 0) {
                StringBuilder domains = new StringBuilder();
                for (String domain : nationListResponse.getTopLevelDomain()) {
                    domains.append(domain + ",\t");
                }
                dbCountryDetail.setTopLevelDomain(domains.toString());
            }
            if (nationListResponse.getBorders() != null && nationListResponse.getBorders().size() > 0) {
                StringBuilder borders = new StringBuilder();
                for (String border : nationListResponse.getBorders()) {
                    borders.append(border + ",\t");
                }
                dbCountryDetail.setBorders(borders.toString());
            }
            if (nationListResponse.getCallingCodes() != null && nationListResponse.getCallingCodes().size() > 0) {
                StringBuilder codes = new StringBuilder();
                for (String code : nationListResponse.getCallingCodes()) {
                    codes.append(code + ",\t");
                }
                dbCountryDetail.setCallingCode(codes.toString());
            }
            dbCountryDetail.setSubRegion(nationListResponse.getSubregion());
            dbCountryDetail.setRegion(nationListResponse.getRegion());
            dbManager.addNationToDb(dbCountryDetail);
        }
    }

    @Override
    public List<DBCountryDetail> getCountries() {
        return dbManager.getAllcountries();
    }
}