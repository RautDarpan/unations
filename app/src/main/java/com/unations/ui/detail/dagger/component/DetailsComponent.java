package com.unations.ui.detail.dagger.component;

import com.unations.application.component.ApiComponent;
import com.unations.ui.countries.NationListActivity;
import com.unations.ui.countries.dagger.module.NationListModule;
import com.unations.ui.countries.dagger.scope.NationListScope;
import com.unations.ui.detail.CountryDetailsActivity;
import com.unations.ui.detail.module.DetailsModule;

import dagger.Component;

/**
 * Created by pleximus on 06/05/17.
 */
@NationListScope
@Component(modules = DetailsModule.class, dependencies = ApiComponent.class)
public interface DetailsComponent {
    void inject(CountryDetailsActivity countryDetailsActivity);
}
