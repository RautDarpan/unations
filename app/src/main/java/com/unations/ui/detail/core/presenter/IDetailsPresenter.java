package com.unations.ui.detail.core.presenter;

import com.unations.core.model.DBCountryDetail;
import com.unations.core.model.NationListResponse;

import java.util.List;

/**
 * Created by 3543 on 08-11-2017.
 */

public interface IDetailsPresenter {

    void displayDetails(DBCountryDetail dbCountryDetails);
}
