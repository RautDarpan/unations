package com.unations.ui.detail;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.unations.NationApplication;
import com.unations.R;
import com.unations.core.model.DBCountryDetail;
import com.unations.ui.detail.core.presenter.DetailsPresenterImpl;
import com.unations.ui.detail.core.presenter.IDetailsPresenter;
import com.unations.ui.detail.core.view.IDetailsView;
import com.unations.ui.detail.dagger.component.DaggerDetailsComponent;
import com.unations.ui.detail.module.DetailsModule;
import com.unations.utils.SvgDecoder;
import com.unations.utils.SvgDrawableTranscoder;
import com.unations.utils.SvgSoftwareLayerSetter;

import java.io.InputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryDetailsActivity extends AppCompatActivity implements IDetailsView {


    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private Context context;
    @Inject
    IDetailsPresenter iNationPresenter;
    @Inject
    DetailsPresenterImpl nationPresenter;

    @BindView(R.id.iv_flag)
    ImageView ivFlag;

    @BindView(R.id.lbl_country_name)
    TextView lblCountryName;
    @BindView(R.id.lbl_country_capital)
    TextView lblCountryCapital;

    @BindView(R.id.lbl_region)
    TextView lblRegion;
    @BindView(R.id.lbl_sub_region)
    TextView lblSubRegion;
    @BindView(R.id.lbl_calling_code)
    TextView lblCallingCodes;
    @BindView(R.id.lbl_domains)
    TextView lblDomains;

    DBCountryDetail dbCountryDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_details);
        ButterKnife.bind(this);
        context = this;
        dbCountryDetail = (DBCountryDetail) getIntent().getSerializableExtra("details");
        // Initialise Dagger
        DaggerDetailsComponent.builder()
                .apiComponent(NationApplication.getAppComponent())
                .detailsModule(new DetailsModule(this))
                .build()
                .inject(this);

        if (dbCountryDetail != null) {
            nationPresenter.displayDetails(dbCountryDetail);
        }
    }

    @Override
    public void onDisplay(DBCountryDetail dbCountryDetail) {

        lblCountryName.setText("@COUNTRY : " + dbCountryDetail.getCountryName());
        lblCountryCapital.setText("@CAPITAL : " + dbCountryDetail.getCountryCapital());
        lblRegion.setText(dbCountryDetail.getRegion());
        lblSubRegion.setText(dbCountryDetail.getSubRegion());
        lblCallingCodes.setText(dbCountryDetail.getCallingCode());
        lblDomains.setText(dbCountryDetail.getTopLevelDomain());
        Uri uri = Uri.parse(dbCountryDetail.getCountryFlagImg());


        requestBuilder = Glide.with(context)
                .using(Glide.buildStreamModelLoader(Uri.class, context), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());

        if (dbCountryDetail.getCountryFlagImg() != null) {
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    // SVG cannot be serialized so it's not worth to cache it
                    .load(uri)
                    .into(ivFlag);
        }
    }
}
