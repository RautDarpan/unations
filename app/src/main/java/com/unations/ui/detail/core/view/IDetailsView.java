package com.unations.ui.detail.core.view;

import com.unations.core.model.DBCountryDetail;

import java.util.List;

/**
 * Created by 3543 on 08-11-2017.
 */

public interface IDetailsView {

    void onDisplay(DBCountryDetail dbCountryDetail);

}
